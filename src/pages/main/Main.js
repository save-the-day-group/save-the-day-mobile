import React from 'react';
import { Text, View, Image, TouchableOpacity } from 'react-native';
import styles from './styles';

import LogoText from '../../assets/lineartext.png';
import { FlatList } from 'react-native-gesture-handler';

export default function Main() {
    const ongs = [
        {
            id: 1,
            name: 'First Ong',
            description: 'We need food for our kitties',
            whatsapp: '123123123',
            email: 'email@email.com'
        },
        {
            id: 2,
            name: 'Second Ong',
            description: 'We need food for our puppies',
            whatsapp: '999988887777',
            email: 'email2@email.com'
        }
    ]
    return (
        <View style={styles.container}>
            <View style={styles.header}>
               <Image source={LogoText} style={styles.img} />
            </View>
            <Text style={styles.welcome}>Bem-Vindo!</Text>
            <Text style={styles.subtitle}>Navegue pelos casos abaixo e salve o dia de alguém!</Text>

            <FlatList 
                data={ongs}
                keyExtractor={ong => ong.id}
                style={styles.ongsList}
                renderItem={({item: ong}) => ( 
                    <View style={styles.ongs}>
                        <Text style={styles.ongName}>{ong.name}</Text>
                        <Text style={styles.ongDescription}>{ong.description}</Text>
                        <TouchableOpacity 
                            style={styles.detailsBtn}
                            onPress={() => {}}
                        >
                            <Text style={styles.textBtn}>Ver Detalhes</Text>
                        </TouchableOpacity>
                    </View>
                )}
            />
        </View>
    );
}
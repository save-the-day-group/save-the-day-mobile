import { StyleSheet } from 'react-native';
import Constants from 'expo-constants';

export default StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: Constants.statusBarHeight + 5,
        backgroundColor: '#ddd',
        paddingHorizontal: 10
    },
    header: {
        alignItems: 'center',
        backgroundColor: '#ff0054',
        height: 120
    },
    img: {
        maxHeight: 110
    },
    welcome: {
        fontSize: 30,
        fontWeight: 'bold',
        textAlign: 'center',
        paddingTop: 10
    },
    subtitle: {
        fontSize: 20,
        textAlign: 'center'
    },
    ongsList: {
        paddingTop: 20,
        paddingHorizontal: 5
    },
    ongs: {
        backgroundColor: '#d9bfb5',
        marginBottom: 10,
        borderRadius: 5,
        padding: 20,
        alignItems: 'center'
    },
    ongName: {
        fontSize: 18,
        fontWeight: 'bold',
        
    },
    ongDescription: {
        fontSize: 15,
    },
    detailsBtn: {
        height: 40,
        width: '90%',
        backgroundColor: '#3d57b1',
        borderRadius: 5,
        marginTop: 10,
    },
    textBtn: {
        lineHeight: 40,
        fontSize: 18,
        textAlign: 'center',
        color: '#fff'
    }
});
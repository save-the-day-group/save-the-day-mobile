import React from 'react';
import { Stylesheet, Text, View, Image, TouchableOpacity, Switch } from 'react-native';

import styles from './styles';
import LogoText from '../../assets/lineartext.png';

export default function Profile() {
    return (
        <View style={styles.container}>
            <View style={styles.header}>
               <Image source={LogoText} style={styles.img} />
            </View>

            <Text style={styles.title}>Perfil</Text>
            <View style={styles.fields}>
                <Text style={styles.label}>Nome: </Text>
                <Text style={styles.value}>John Taylor</Text>
            </View>
            
            <View style={styles.fields}>
                <Text style={styles.label}>Email:  </Text>
                <Text style={styles.value}>john@email.com</Text>
            </View>
            <View style={styles.editRegion}>
                <TouchableOpacity
                    style={styles.editBtn}
                    onPress={() => {}}
                >
                    <Text style={styles.editTxt}>Editar Perfil</Text>
                </TouchableOpacity>
            </View>

            <View style={styles.switchView}>
                <Text style={styles.switchText}>Receber notificações</Text>
                <Switch style={styles.theSwitch} />
            </View>
        </View>
    );
}
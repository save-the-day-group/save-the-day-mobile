import { StyleSheet } from 'react-native';
import Constants from 'expo-constants';

export default StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: Constants.statusBarHeight + 5,
        backgroundColor: '#ddd',
        paddingHorizontal: 10
    },
    header: {
        alignItems: 'center',
        backgroundColor: '#ff0054',
        height: 120
    },
    img: {
        maxHeight: 110
    },
    title: {
        fontSize: 24,
        fontWeight: 'bold',
        marginTop: 20,
        marginBottom: 20,
        textAlign: 'center'
    },
    label: {
        fontSize: 18,
        fontWeight: 'bold',
        marginBottom: 10
    },
    fields: {
        flexDirection: 'row'
    },
    value: {
        fontSize: 18
    },
    editBtn: {
        height: 40,
        width: '90%',
        backgroundColor: '#3d57b1',
        borderRadius: 5,
        marginTop: 60,
    },
    editTxt: {
        lineHeight: 40,
        fontSize: 22,
        textAlign: 'center',
        color: '#fff'
    },
    switchView: {
        flexDirection: 'row',
        marginTop: 30,
        alignItems: 'center',
        justifyContent: 'space-around'
    },
    switchText: {
        fontSize: 20,
        marginRight: 10,
        marginTop: 5
    },
    editRegion: {
        alignItems: 'center'
    },
    theSwitch: {
        
    }
});
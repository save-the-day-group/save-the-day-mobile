import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Ionicons } from '@expo/vector-icons';

import Main from './pages/main/Main';
import Profile from './pages/profile/Profile'

const BottomTab = createBottomTabNavigator();

export default function Routes() {
    return(
        <NavigationContainer>
            <BottomTab.Navigator screenOptions={({route}) => ({
                tabBarIcon: ({ focused, color, size }) => {
                    let iconName;

                    if(route.name == 'Home') {
                        iconName = 'ios-home';
                        focused = color ? 'ff0054' : "#000"
                       
                    } else if (route.name == 'Profile') {
                        iconName = 'ios-person';
                        focused = color ? 'ff0054' : "#000"
                    }
                    
                    return <Ionicons name={iconName} size={20} color={color} />;
                },
            })}
                tabBarOptions = {{
                    activeTintColor: '#ff0054',
                    inactiveTintColor: 'black'
                }}
            >
                <BottomTab.Screen name="Home" component={Main} />
                <BottomTab.Screen name="Profile" component={Profile} />
            </BottomTab.Navigator>
        </NavigationContainer>
    );
}